# MAI Lesson 1 – List, Dict, Set
## Работа с объектами – список, словарь, множетсва

#### HomeWork
1. Потренироваться писать код на примере кейса с квартирами
2. Поиграть в "[Угадай число](http://www.mathtask.ru/0047-game-01.php)"
	- найти и описать несколько (минимум 2) стратегии, как в нее можно выигрывать быстрее
	- Старайтесь описывать так, чтобы это было похоже на алгоритм. В идеале - напишите псевдокод

#### Helpful
- List `[ , ]` – [Списки](https://pythonworld.ru/tipy-dannyx-v-python/spiski-list-funkcii-i-metody-spiskov.html)
- Dict `{ : }` – [Словари](https://pythonworld.ru/tipy-dannyx-v-python/slovari-dict-funkcii-i-metody-slovarej.html)
- Set `{ , }` – [множества](https://pythonworld.ru/tipy-dannyx-v-python/mnozhestva-set-i-frozenset.html)
- Tuple `( , )` – [Кортежи](https://pythonworld.ru/tipy-dannyx-v-python/kortezhi-tuple.html)
- f-Strings – [f-строки](https://realpython.com/python-f-strings)
- VennDiagram – [Диаграмма Венна](https://ru.wikipedia.org/wiki/Диаграмма_Венна)

---

#### Стратегии игры "[Угадай число](http://www.mathtask.ru/0047-game-01.php)"
**1. Способ**
- Понять, какие числа присутствют:
	- <code>0123</code>
	- <code>4567</code>
	- <code>89</code> + попытаться угадать 2 цифры из прошлых результатов
- Совместить цифры в наиболее удачных комбинациях
- Подбирать оставшиеся цифры

**2. Способ**
- Понять, какие числа присутствют:
	- <code>0000</code>
	- <code>1111</code>
	- <code>....</code>
	- <code>9999</code>
- Совместить цифры в наиболее удачных комбинациях
- Подбирать оставшиеся цифры

**3. Способ**

> ![](./assets/1.png)

> ![](./assets/2.png)